package pl.piotr.artistinfoapp

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import pl.piotr.artistinfoapp.di.AppInjector
import javax.inject.Inject

class GlobalApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AppInjector.initialize(this)
    }

    override fun activityInjector() = dispatchingAndroidInjector
}