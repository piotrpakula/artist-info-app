package pl.piotr.artistinfoapp.api

data class DataApi<T>(
    val data: List<T> = listOf(),
    val total: Int
)