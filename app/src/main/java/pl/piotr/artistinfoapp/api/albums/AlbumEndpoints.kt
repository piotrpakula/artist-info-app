package pl.piotr.artistinfoapp.api.albums

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface AlbumEndpoints {

    @GET("/album/{id}")
    suspend fun getAlbum(@Path("id") albumId: String): Response<AlbumModel>
}