package pl.piotr.artistinfoapp.api.albums

import com.google.gson.annotations.SerializedName
import pl.piotr.artistinfoapp.api.DataApi

data class AlbumModel(
    val id: String,
    val title: String? = null,
    @SerializedName("cover_big")
    val coverBig: String? = null,
    val tracks: DataApi<TrackModel>? = null
)