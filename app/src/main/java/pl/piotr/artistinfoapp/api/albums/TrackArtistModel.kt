package pl.piotr.artistinfoapp.api.albums

data class TrackArtistModel(
    val id: String,
    val name: String? = null
)