package pl.piotr.artistinfoapp.api.albums

data class TrackModel(
    val id: String,
    val title: String? = null,
    val artist: TrackArtistModel? = null,
    val duration: Long? = null
)