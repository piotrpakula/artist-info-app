package pl.piotr.artistinfoapp.api.artists

import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.api.DataApi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ArtistEndpoints {

    @GET("/artist/{artistId}/albums")
    suspend fun getArtistAlbums(@Path("artistId") artistId: String): Response<DataApi<AlbumModel>>
}