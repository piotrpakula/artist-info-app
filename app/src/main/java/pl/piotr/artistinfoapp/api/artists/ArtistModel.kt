package pl.piotr.artistinfoapp.api.artists

data class ArtistModel(
    val id: String,
    val name: String? = null,
    val picture: String? = null
)