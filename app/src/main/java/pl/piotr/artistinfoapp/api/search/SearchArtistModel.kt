package pl.piotr.artistinfoapp.api.search

import pl.piotr.artistinfoapp.api.artists.ArtistModel

data class SearchArtistModel(
    val id: String,
    val artist: ArtistModel
)