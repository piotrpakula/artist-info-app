package pl.piotr.artistinfoapp.api.search

import pl.piotr.artistinfoapp.api.DataApi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchEndpoints {

    @GET("/search")
    suspend fun searchArtistByName(@Query("q") query: String? = null): Response<DataApi<SearchArtistModel>>
}