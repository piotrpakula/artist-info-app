package pl.piotr.artistinfoapp.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pl.piotr.artistinfoapp.GlobalApplication
import pl.piotr.artistinfoapp.di.modules.ApiModule
import pl.piotr.artistinfoapp.di.modules.AppModule
import pl.piotr.artistinfoapp.di.modules.MainActivityModule
import pl.piotr.artistinfoapp.di.modules.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ApiModule::class,
        MainActivityModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: GlobalApplication)
}