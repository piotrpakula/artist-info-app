package pl.piotr.artistinfoapp.di.modules

import dagger.Module
import dagger.Provides
import pl.piotr.artistinfoapp.api.albums.AlbumEndpoints
import pl.piotr.artistinfoapp.api.artists.ArtistEndpoints
import pl.piotr.artistinfoapp.api.search.SearchEndpoints
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideArtistApi(retrofit: Retrofit) = retrofit.create(ArtistEndpoints::class.java)

    @Singleton
    @Provides
    fun provideAlbumApi(retrofit: Retrofit) = retrofit.create(AlbumEndpoints::class.java)

    @Singleton
    @Provides
    fun provideSearchApi(retrofit: Retrofit) = retrofit.create(SearchEndpoints::class.java)
}