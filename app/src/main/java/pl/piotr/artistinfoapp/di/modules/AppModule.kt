package pl.piotr.artistinfoapp.di.modules

import dagger.Module
import dagger.Provides
import pl.piotr.artistinfoapp.api.ApiService
import javax.inject.Singleton


@Module
class AppModule {

    @Singleton
    @Provides
    fun provideRetrofit() = ApiService().retrofit
}
