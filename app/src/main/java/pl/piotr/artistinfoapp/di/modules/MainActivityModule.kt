package pl.piotr.artistinfoapp.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.piotr.artistinfoapp.main.MainActivity
import pl.piotr.artistinfoapp.main.albums.AlbumsFragment
import pl.piotr.artistinfoapp.main.details.AlbumDetailsFragment
import pl.piotr.artistinfoapp.main.search.SearchFragment

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun contributeAlbumsFragment(): AlbumsFragment

    @ContributesAndroidInjector
    abstract fun contributeAlbumDetailsFragment(): AlbumDetailsFragment
}