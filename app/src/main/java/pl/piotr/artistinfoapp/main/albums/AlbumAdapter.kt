package pl.piotr.artistinfoapp.main.albums

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.piotr.artistinfoapp.R
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.databinding.ItemAlbumBinding
import pl.piotr.artistinfoapp.main.albums.AlbumsFragmentDirections.Companion.actionAlbumsToAlbumDetails

class AlbumAdapter : ListAdapter<AlbumModel, AlbumAdapter.ViewHolder>(AlbumsDiffCallback()) {

    private var artistName = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_album,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) { bind(getItem(position), artistName) }
    }

    fun setArtist(artistName: String) {
        this.artistName = artistName
    }

    class ViewHolder(
        private val binding: ItemAlbumBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.containerAlbums.setOnClickListener {
                binding.model?.let { album ->
                    navigateToAlbumDetails(album, binding.artist, it)
                }
            }
        }

        fun bind(album: AlbumModel, artistName: String) {
            with(binding) {
                model = album
                artist = artistName
                executePendingBindings()
            }
        }

        private fun navigateToAlbumDetails(model: AlbumModel, artistName: String?, view: View) {
            view.findNavController()
                .navigate(actionAlbumsToAlbumDetails(model.id, artistName))
        }
    }
}

private class AlbumsDiffCallback : DiffUtil.ItemCallback<AlbumModel>() {

    override fun areItemsTheSame(
        oldItem: AlbumModel,
        newItem: AlbumModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: AlbumModel,
        newItem: AlbumModel
    ): Boolean {
        return oldItem == newItem
    }
}