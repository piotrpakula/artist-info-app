package pl.piotr.artistinfoapp.main.albums

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.databinding.FragmentAlbumsBinding
import pl.piotr.artistinfoapp.di.Injectable
import javax.inject.Inject

class AlbumsFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AlbumsViewModel
    lateinit var binding: FragmentAlbumsBinding

    private val args: AlbumsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, viewModelFactory).get(AlbumsViewModel::class.java)

        val adapter = AlbumAdapter()
        binding = FragmentAlbumsBinding.inflate(inflater, container, false).apply {
            model = viewModel
            albumsResults.adapter = adapter
            lifecycleOwner = viewLifecycleOwner
            viewModel.initialize(args.artistId, args.artistName)

            (activity as? AppCompatActivity)?.setSupportActionBar(animToolbar)
        }

        subscribeUi(adapter)

        return binding.root
    }

    private fun subscribeUi(adapter: AlbumAdapter) {
        viewModel.albums.observe(this, Observer<List<AlbumModel>> { value ->
            adapter.setArtist(viewModel.artistName)
            adapter.submitList(value)
        })
    }
}