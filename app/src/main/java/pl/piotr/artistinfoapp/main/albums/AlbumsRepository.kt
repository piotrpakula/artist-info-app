package pl.piotr.artistinfoapp.main.albums

import pl.piotr.artistinfoapp.api.DataApi
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.api.artists.ArtistEndpoints
import retrofit2.Response
import javax.inject.Inject

class AlbumsRepository @Inject constructor(
    private val artistEndpoints: ArtistEndpoints
) {
    suspend fun getArtistAlbums(artistId : String): Response<DataApi<AlbumModel>> {
       return artistEndpoints.getArtistAlbums(artistId)
    }
}