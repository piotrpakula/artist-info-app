package pl.piotr.artistinfoapp.main.albums

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import javax.inject.Inject

class AlbumsViewModel  @Inject constructor(
    private val albumsRepository: AlbumsRepository
) : ViewModel() {

    var artistName = ""
    var isLoading = MutableLiveData(false)
    val albums = MutableLiveData<List<AlbumModel>>()

    fun initialize(artistId: String?, artistName: String?) {
        artistId ?: return
        this.artistName = artistName ?: ""

        viewModelScope.launch {
            isLoading.value = true

            albumsRepository.getArtistAlbums(artistId).body()?.data?.let {
                delay(200)
                albums.value = it
                isLoading.value = false
            }
        }
    }
}