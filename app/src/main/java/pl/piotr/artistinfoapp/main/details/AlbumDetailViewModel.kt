package pl.piotr.artistinfoapp.main.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import javax.inject.Inject

class AlbumDetailViewModel @Inject constructor(
    private val albumDetailsRepository: AlbumDetailsRepository
) : ViewModel() {

    var isLoading = MutableLiveData(false)
    var artistName = MutableLiveData<String>()
    val album = MutableLiveData<AlbumModel>()

    fun initialize(albumId: String?, artistName: String?) {
        albumId ?: return
        isLoading.value = true
        this.artistName.value = artistName ?: ""

        viewModelScope.launch {
            albumDetailsRepository.getAlbum(albumId).body()?.let {
                delay(250)
                album.value = it
            }
            isLoading.value = false
        }
    }
}