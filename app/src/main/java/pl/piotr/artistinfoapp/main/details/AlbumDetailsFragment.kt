package pl.piotr.artistinfoapp.main.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.databinding.FragmentAlbumDetailsBinding
import pl.piotr.artistinfoapp.di.Injectable
import javax.inject.Inject

class AlbumDetailsFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AlbumDetailViewModel
    lateinit var binding: FragmentAlbumDetailsBinding

    private val args: AlbumDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, viewModelFactory).get(AlbumDetailViewModel::class.java)

        val adapter = TrackAdapter()
        binding = FragmentAlbumDetailsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            model = viewModel
            tracksResults.adapter = adapter
            viewModel.initialize(args.albumId, args.albumArtistName)

            (activity as? AppCompatActivity)?.apply {
                setSupportActionBar(animToolbar)
                supportActionBar?.setDisplayShowTitleEnabled(false)
            }
        }
        subscribeUi(adapter)

        return binding.root
    }

    private fun subscribeUi(adapter: TrackAdapter) {
        viewModel.album.observe(this, Observer<AlbumModel> { value ->
            adapter.submitList(value.tracks?.data)
        })
    }
}