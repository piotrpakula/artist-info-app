package pl.piotr.artistinfoapp.main.details

import pl.piotr.artistinfoapp.api.albums.AlbumEndpoints
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import retrofit2.Response
import javax.inject.Inject

class AlbumDetailsRepository @Inject constructor(
    private val albumEndpoints: AlbumEndpoints
) {
    suspend fun getAlbum(albumId: String): Response<AlbumModel> {
        return albumEndpoints.getAlbum(albumId)
    }
}