package pl.piotr.artistinfoapp.main.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.piotr.artistinfoapp.R
import pl.piotr.artistinfoapp.api.albums.TrackModel
import pl.piotr.artistinfoapp.databinding.ItemTrackBinding

class TrackAdapter : ListAdapter<TrackModel, TrackAdapter.ViewHolder>(AlbumsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_track,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) { bind(getItem(position), position) }
    }

    class ViewHolder(
        private val binding: ItemTrackBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(track: TrackModel, position : Int) {
            with(binding) {
                model = track
                number = (position + 1).toString()
                executePendingBindings()
            }
        }

    }
}

private class AlbumsDiffCallback : DiffUtil.ItemCallback<TrackModel>() {

    override fun areItemsTheSame(
        oldItem: TrackModel,
        newItem: TrackModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: TrackModel,
        newItem: TrackModel
    ): Boolean {
        return oldItem == newItem
    }
}