package pl.piotr.artistinfoapp.main.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.piotr.artistinfoapp.R
import pl.piotr.artistinfoapp.api.search.SearchArtistModel
import pl.piotr.artistinfoapp.databinding.ItemSearchBinding
import pl.piotr.artistinfoapp.main.search.SearchFragmentDirections.Companion.actionSearchToAlbums
import pl.piotr.artistinfoapp.tools.hideKeyboardFrom

class SearchArtistAdapter :
    ListAdapter<SearchArtistModel, SearchArtistAdapter.ViewHolder>(SearchArtistDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_search,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) { bind(getItem(position)) }
    }

    class ViewHolder(
        private val binding: ItemSearchBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.containerSearchArtist.setOnClickListener {
                binding.model?.let { artist ->
                    navigateToAlbums(artist, it)
                }
            }
        }

        fun bind(searchArtistModel: SearchArtistModel) {
            with(binding) {
                model = searchArtistModel
                executePendingBindings()
            }
        }

        private fun navigateToAlbums(model: SearchArtistModel, view: View) {
            hideKeyboardFrom(view.context, view)
            view.findNavController()
                .navigate(actionSearchToAlbums(model.artist.id, model.artist.name))
        }
    }
}

private class SearchArtistDiffCallback : DiffUtil.ItemCallback<SearchArtistModel>() {

    override fun areItemsTheSame(
        oldItem: SearchArtistModel,
        newItem: SearchArtistModel
    ): Boolean {
        return oldItem.artist.id == newItem.artist.id
    }

    override fun areContentsTheSame(
        oldItem: SearchArtistModel,
        newItem: SearchArtistModel
    ): Boolean {
        return oldItem == newItem
    }
}