package pl.piotr.artistinfoapp.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import pl.piotr.artistinfoapp.api.search.SearchArtistModel
import pl.piotr.artistinfoapp.databinding.FragmentSearchBinding
import pl.piotr.artistinfoapp.di.Injectable
import javax.inject.Inject

class SearchFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: SearchViewModel
    lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, viewModelFactory).get(SearchViewModel::class.java)
        val adapter = SearchArtistAdapter()

        binding = FragmentSearchBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            model = viewModel
            searchResults.adapter = adapter
            search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean = true
                override fun onQueryTextChange(newText: String?): Boolean = true.also {
                    viewModel.search(newText)
                }
            })
        }

        subscribeUi(adapter)

        return binding.root
    }

    private fun subscribeUi(adapter: SearchArtistAdapter) {
        viewModel.artists.observe(this, Observer<List<SearchArtistModel>> { value ->
            adapter.submitList(value)
        })
    }
}