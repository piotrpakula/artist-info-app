package pl.piotr.artistinfoapp.main.search

import pl.piotr.artistinfoapp.api.DataApi
import pl.piotr.artistinfoapp.api.search.SearchArtistModel
import pl.piotr.artistinfoapp.api.search.SearchEndpoints
import retrofit2.Response
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val searchEndpoints: SearchEndpoints
) {
    suspend fun searchByArtistName(name: String): Response<DataApi<SearchArtistModel>> {
        return searchEndpoints.searchArtistByName(name)
    }
}