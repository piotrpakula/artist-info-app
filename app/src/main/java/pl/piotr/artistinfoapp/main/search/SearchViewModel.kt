package pl.piotr.artistinfoapp.main.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import pl.piotr.artistinfoapp.api.search.SearchArtistModel
import javax.inject.Inject


class SearchViewModel @Inject constructor(
    private val searchRepository: SearchRepository
) : ViewModel() {

    private var job: Job? = null

    var isLoading = MutableLiveData(false)
    val artists = MutableLiveData<List<SearchArtistModel>>()

    fun search(query: String?) {
        if (job != null || job?.isActive == true) {
            job?.cancel()
            job = null
        }

        isLoading.value = true
        job = viewModelScope.launch {
            val name = query?.trim() ?: ""
            delay(300)
            searchRepository.searchByArtistName(name).body()?.data
                ?.distinctBy { it.artist.id }
                ?.let { artists.value = it }
                ?: run { artists.value = emptyList() }
            job = null
            isLoading.value = false
        }
    }
}