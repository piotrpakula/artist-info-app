package pl.piotr.artistinfoapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.MediaType.get
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import pl.piotr.artistinfoapp.api.DataApi
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.api.albums.TrackModel
import pl.piotr.artistinfoapp.main.details.AlbumDetailViewModel
import pl.piotr.artistinfoapp.main.details.AlbumDetailsRepository
import pl.piotr.artistinfoapp.utilities.getValue
import retrofit2.Response

class AlbumDetailViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val albumDetailsRepository: AlbumDetailsRepository = mock()

    private lateinit var viewModel: AlbumDetailViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() = runBlocking {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = AlbumDetailViewModel(albumDetailsRepository)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Initialize album details with correct data, album has succeed initialize`() = runBlocking {
        whenever(albumDetailsRepository.getAlbum(albumId))
            .doReturn(Response.success(responseAlbumsMock))

        viewModel.initialize(albumId, artistName)

        assertEquals(responseAlbumsMock, getValue(viewModel.album))
        assertEquals(artistName, getValue(viewModel.artistName))
    }

    @Test
    fun `Initialize album details with correct data, server error`() = runBlocking {
        whenever(albumDetailsRepository.getAlbum(albumId))
            .doReturn(Response.error(500, ResponseBody.create(get("application/json"), "{}")))

        viewModel.initialize(albumId, artistName)

        assertEquals(artistName, getValue(viewModel.artistName))
        assertNull(getValue(viewModel.album))
    }

    @Test
    fun `Initialize album details with no input data, nothing happen`() = runBlocking {
        viewModel.initialize(null, null)

        assertNull(getValue(viewModel.album))
        assertNull(getValue(viewModel.artistName))
    }

    companion object {

        private const val albumId = "1000"
        private const val artistName = "Peja"

        private val responseAlbumsMock = AlbumModel(
            "10",
            "Slums Attack",
            "url",
            DataApi(listOf(TrackModel("100", "997"), TrackModel("110", "Śmierć")), 2)
        )
    }
}