package pl.piotr.artistinfoapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.MediaType.get
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import pl.piotr.artistinfoapp.api.DataApi
import pl.piotr.artistinfoapp.api.albums.AlbumModel
import pl.piotr.artistinfoapp.main.albums.AlbumsRepository
import pl.piotr.artistinfoapp.main.albums.AlbumsViewModel
import pl.piotr.artistinfoapp.utilities.getValue
import retrofit2.Response

class AlbumsViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val albumsRepository: AlbumsRepository = mock()

    private lateinit var viewModel: AlbumsViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() = runBlocking {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = AlbumsViewModel(albumsRepository)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Initialize artist albums with correct data, albums has succeed initialize`() =
        runBlocking {
            whenever(albumsRepository.getArtistAlbums(artistId))
                .doReturn(Response.success(responseAlbumsMock))

            viewModel.initialize(artistId, artistName)

            assertEquals(responseAlbumsMock.data, getValue(viewModel.albums))
            assertEquals(artistName, viewModel.artistName)
        }

    @Test
    fun `Initialize artist albums with correct data, server error`() = runBlocking {
        whenever(albumsRepository.getArtistAlbums(artistId))
            .doReturn(Response.error(500, ResponseBody.create(get("application/json"), "{}")))

        viewModel.initialize(artistId, artistName)

        assertEquals(artistName, viewModel.artistName)
        assertNull(getValue(viewModel.albums))
    }

    @Test
    fun `Initialize artist albums with no input data, nothing happen`() = runBlocking {
        viewModel.initialize(null, null)

        assertNull(getValue(viewModel.albums))
        assertTrue(viewModel.artistName.isEmpty())
    }

    companion object {

        private const val artistId = "0"
        private const val artistName = "Peja"

        private val responseAlbumsMock = DataApi(
            listOf(
                AlbumModel("10", "Slums Attack"),
                AlbumModel("20", "GOAT")
            ), 2
        )
    }
}