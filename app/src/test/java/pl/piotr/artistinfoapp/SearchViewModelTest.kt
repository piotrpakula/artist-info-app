package pl.piotr.artistinfoapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.MediaType.get
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import pl.piotr.artistinfoapp.api.DataApi
import pl.piotr.artistinfoapp.api.artists.ArtistModel
import pl.piotr.artistinfoapp.api.search.SearchArtistModel
import pl.piotr.artistinfoapp.main.search.SearchRepository
import pl.piotr.artistinfoapp.main.search.SearchViewModel
import pl.piotr.artistinfoapp.utilities.getValue
import retrofit2.Response

class SearchViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val searchRepository: SearchRepository = mock()

    private lateinit var viewModel: SearchViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() = runBlocking {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = SearchViewModel(searchRepository)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Search artist with correct query, artist found`() = runBlocking {
        whenever(searchRepository.searchByArtistName("Peja")).doAnswer {
            Response.success(responseSingleArtistMock)
        }

        viewModel.search("Peja")

        assertEquals(responseSingleArtistMock.data, getValue(viewModel.artists))
    }

    @Test
    fun `Search artist with correct query, server error`() = runBlocking {
        whenever(searchRepository.searchByArtistName(""))
            .doReturn(Response.error(500, ResponseBody.create(get("application/json"), "{}")))

        viewModel.search("")

        assertTrue(getValue(viewModel.artists).isEmpty())
    }

    @Test
    fun `Search duplicated artist with correct query, returned one artist`() = runBlocking {
        whenever(searchRepository.searchByArtistName("Peja"))
            .doReturn(Response.success(responseDuplicateArtistMock))

        viewModel.search("Peja")

        assertNotEquals(responseDuplicateArtistMock.data, getValue(viewModel.artists))
        assertEquals(1, getValue(viewModel.artists).size)
    }

    @Test
    fun `Search artist with wrong query, artist has not found`() = runBlocking {
        whenever(searchRepository.searchByArtistName(""))
            .doReturn (Response.success(DataApi(listOf(), 0)))

        viewModel.search("")

        assertEquals(0, getValue(viewModel.artists).size)
    }

    companion object {

        val responseSingleArtistMock = DataApi(
            listOf(
                SearchArtistModel("0", ArtistModel("10", "Peja"))
            ), 1
        )

        val responseDuplicateArtistMock = DataApi(
            listOf(
                SearchArtistModel("0", ArtistModel("10", "Peja")),
                SearchArtistModel("0", ArtistModel("10", "Peja"))
            ), 2
        )
    }
}